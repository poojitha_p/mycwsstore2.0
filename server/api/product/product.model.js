'use strict';

import mongoose from 'mongoose';

var ProductSchema = new mongoose.Schema({
  title: String,
  code: String,
  subtitle: String,
  description: String,
  avaiable: Boolean,
  category: {
    type: 'String',
    enum: ['men', 'women', 'unisex', 'kids']
  },
  subCategory: {
    type: 'String',
    enum: ['tops', 'bottoms']
  },
  sizes: [{
    type: String,
    enum : ['small', 'medium', 'large', 'extra-large']
  }],
  fits: [{
    type: String,
    enum : ['regular', 'slim-fit', 'tailored']
  }],
  styles: [{
    type: String,
    enum : ['formal', 'semi-formal', 'casual', 'party-wear', 'ethnic']
  }],
  patterns: [{
    type: String,
    enum : ['solid', 'checkered', 'striped', 'pin-striped', 'printed']
  }],
  material: [{
    type: String,
    enum: ['cotton', 'polyester', 'linen', 'khadi' , 'wool']
  }]
});

//size
//category
//fit
//color
//material
//stock
//price

export default mongoose.model('Product', ProductSchema);
