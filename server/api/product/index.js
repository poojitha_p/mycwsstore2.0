'use strict';

var express = require('express');
var controller = require('./product.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', controller.getAllProducts);
router.get('/:productId', controller.getProductById);
router.post('/', auth.hasRole('admin'), controller.createProduct);
router.put('/:productId', auth.hasRole('admin'), controller.updateProduct);
router.patch('/:productId', auth.hasRole('admin'), controller.patchProduct);
router.delete('/:productId', auth.hasRole('admin'), controller.deleteProduct);

module.exports = router;
